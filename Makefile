# Compiler and flags
CXX = g++
CXXFLAGS = -Wall -Wextra -I ./include
LDFLAGS =
LDLIBS = -lcurl -lpthread
UTILS = utils
ASAN_FLAGS = -fsanitize=address -fno-omit-frame-pointer
ASAN_LDFLAGS = -fsanitize=address

# Source files
SRCS = main.cpp $(wildcard $(UTILS)/*.cpp)
OBJS = $(SRCS:.cpp=.o)

# Target name
TARGET = downloader
MEMCHECK_TARGET = $(TARGET)_memcheck

# Default build target
all: $(TARGET)
memcheck: $(MEMCHECK_TARGET)

# Link object files into the target
$(TARGET): $(OBJS)
	$(CXX) $(LDFLAGS) $(LDLIBS) -o $@ $^

$(MEMCHECK_TARGET): $(OBJS)
	$(CXX) $(ASAN_LDFLAGS) $(LDLIBS) -o $@ $^

# Compile source files into object files
%.o: %.cpp
	$(CXX) $(CXXFLAGS) -c $< -o $@

# Clean build products
clean:
	rm -f $(OBJS) $(TARGET) $(MEMCHECK_TARGET)

# Phony targets to force dependency regeneration
.PHONY: clean all memcheck