#include "DownloadConfiguration.h"

DownloadConfiguration::DownloadConfiguration(const std::string& filename) {
    std::ifstream file(filename);
    if (file.is_open()) {
        std::string line;
        while (std::getline(file, line)) {
            // Skip empty lines and comments (starting with '#')
            if (!line.empty() && line[0] != '#') {
                parseLine(line);
            }
        }
        file.close();
    } else {
        std::cerr << "Unable to open file: " << filename << std::endl;
    }
}

std::string DownloadConfiguration::getValue(const std::string& key, const std::string& defaultValue) const {
    if (configData.find(key) != configData.end()) {
        return configData.at(key);
    }
    return defaultValue;
}

void DownloadConfiguration::parseLine(const std::string& line) {
    std::size_t pos = line.find('=');
    if (pos != std::string::npos) {
        std::string key = line.substr(0, pos);
        std::string value = line.substr(pos + 1, line.length());
        configData[key] = value;
    }
}

const std::map<std::string, std::string> &DownloadConfiguration::getConfigData() const {
    return configData;
}
