#include <time.h>
#include <algorithm>
#include <thread>
#include <memory>
#include "DownloadManager.h"

DownloadManager::DownloadManager() {
    m_IsCancel = false;
}

DownloadManager::~DownloadManager() {
    cancelDownload();
}

void DownloadManager::addDownloadTask(std::shared_ptr<DownloadTask> task) {
    tasks.push_back(task);
}

void DownloadManager::clear() {
    tasks.clear();
}

bool DownloadManager::startDownload() {
    curl_global_init(CURL_GLOBAL_ALL);
    for (auto &item: tasks) {
        item->syncDownload();
    }

    bool isDownloading = true;
    while (isDownloading) {
        // Check if all tasks have been completed
        isDownloading = std::any_of(tasks.begin(), tasks.end(), [](std::shared_ptr<DownloadTask> item) {
            return item->IsDownloading();
        });

        sleepMillisecond(100);
    }

    // Integrate the results of all tasks and return.
    bool isDownloadFinish = std::all_of(tasks.begin(), tasks.end(), [](std::shared_ptr<DownloadTask> item) {
        return item->getLastDownloadResult();
    });

    curl_global_cleanup();
    return isDownloadFinish;
}

void DownloadManager::cancelDownload() {
    m_IsCancel = true;
    for (auto &item: tasks) {
        item->cancelDownload();
    }
}

void DownloadManager::sleepMillisecond(int millisecond) const {
    const int span = 20;

    clock_t tmBegin = clock();
    while (true) {
        if (clock() - tmBegin > millisecond) {
            break;
        }

        if (m_IsCancel) {
            break;
        }

        std::this_thread::sleep_for(std::chrono::milliseconds(span));
    }
}