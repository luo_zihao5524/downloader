#include <iostream>
#include <thread>
#include <curl/curl.h>
#include "Util.h"
#include "FileUtil.h"
#include "PackageDownload.h"
#include "DownloadTask.h"

bool PackagePartition::isSuccess1() const {
    return isSuccess;
}

DownloadTask *PackagePartition::getDownloadTask() const {
    return downloadTask;
}

int PackagePartition::getFile() const {
    return file;
}

long long int PackagePartition::getStartPos() const {
    return startPos;
}

void PackagePartition::setStartPos(long long int startPos) {
    PackagePartition::startPos = startPos;
}

long long int PackagePartition::getEndPos() const {
    return endPos;
}

long long int PackagePartition::getDownloadSize() const {
    return downloadSize;
}

bool PackagePartition::downloadData() {
    bool result = false;

    /* Open the file when download stat, and close the file after each thread completes its respective download task. */
    this->file = open(tmpFileName.c_str(), O_RDWR | O_CREAT, 0666);

    /* Set the starting position for writing files */
    if (lseek(this->file, this->startPos, SEEK_SET) == Util::INVALID_FD) {
        std::cerr << "cannot set offset" << std::endl;
        close(this->file);
        return false;
    }
    /* Retries up to 30 times. */
    for (int i = 0; i < 30 && !isCancel; i++) {
        this->curl = this->downloadTask->createCurl(this);
        if (this->curl == nullptr) {
            std::cerr << "init curl failed!" << std::endl;
            break;
        } else {
            CURLcode res = curl_easy_perform(this->curl);

            /* If the download is cancelled proactively, exit the loop and no longer attempt. */
            if (CURLE_ABORTED_BY_CALLBACK == res) {
                (void) curl_easy_cleanup(this->curl);
                break;
            }

            if (res == CURLE_OK) {
                long response_code = 0;
                curl_easy_getinfo(this->curl, CURLINFO_RESPONSE_CODE, &response_code);

                if (404 == response_code || (response_code < 200 || response_code > 300)) {
                    std::cerr << "curl_easy_getinfo failed rescode: " << response_code << std::endl;
                } else {
                    (void) curl_easy_cleanup(this->curl);
                    result = true;
                    this->isSuccess = true;
                    break;
                }
            } else {
                std::cerr << "curl_easy_perform failed res: " << res << ": " << curl_easy_strerror(res) << std::endl;
            }
        }

        (void) curl_easy_cleanup(this->curl);
        std::this_thread::sleep_for(std::chrono::milliseconds(500));
        std::cerr << "retry: " << i + 1 << std::endl;
    }

    /* The download task has been completed, close the file */
    close(this->file);

    return result;
}

size_t PackagePartition::writeCallback(void *data, size_t size, size_t nmemb, void *packageData) {
    size_t realSize = size * nmemb;
    if (nullptr == data || nullptr == packageData) {
        return realSize;
    }

    PackagePartition *packagePartition = static_cast<PackagePartition *>(packageData);

    size_t dwWritten = FileUtil::writeFile(packagePartition->file, data, (unsigned long) realSize);
    packagePartition->downloadSize += dwWritten;
    packagePartition->startPos += dwWritten;
    return dwWritten;
}

int PackagePartition::progressCallback(void *data, curl_off_t totalSize, curl_off_t downloadedSize) {
    if (nullptr == data || 0 == totalSize) {
        return CURLE_OK;
    }

    PackagePartition *packagePartition = static_cast<PackagePartition *>(data);
    DownloadTask *task = packagePartition->downloadTask;

    return task->updateProgress(totalSize, downloadedSize);
}

PackagePartition::PackagePartition(const int packageId, std::string url, DownloadTask *task, int startPos, int endPos,
                                   int totalSize, const std::string tmpFileName) : packageId(packageId), url(url),
                                                                                   downloadTask(task),
                                                                                   startPos(startPos), endPos(endPos),
                                                                                   totalSize(totalSize),
                                                                                   tmpFileName(tmpFileName) {
    isSuccess = false;
    isCancel = false;
    file = Util::INVALID_FD;
    downloadSize = 0;
}

const std::string &PackagePartition::getUrl() const {
    return url;
}

void PackagePartition::setIsCancel(bool isCancel) {
    PackagePartition::isCancel = isCancel;
}

void PackagePartition::addDownloadSize(long long int downloadSize) {
    this->downloadSize += downloadSize;
}

void PackagePartition::addStartPos(long long int startPos) {
    this->startPos += startPos;
}
