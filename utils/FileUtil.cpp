#include <thread>
#include <curl/curl.h>
#include "Util.h"
#include "FileUtil.h"

void FileUtil::truncateFile(const std::string &path) {
    int file = open(path.c_str(), O_WRONLY | O_CREAT | O_TRUNC, 0666);
    if (Util::INVALID_FD != file) {
        close(file);
    }
}

size_t FileUtil::writeFile(int file, const void *buffer, unsigned long size) {
    long long dwWritten = write(file, buffer, size);
    return dwWritten;
}

size_t FileUtil::headerCallback(char *buffer, size_t size, size_t itemsNum, void *packageData) {
    size_t realSize = size * itemsNum;

    std::map<std::string, std::string> *pHeaderContent = (std::map<std::string, std::string> *) packageData;

    std::string strContent = buffer;

    size_t nPos = strContent.find(':');

    if (std::string::npos != nPos) {
        std::string strKey = strContent.substr(0, nPos);

        std::string strValue = strContent.substr(nPos + 2);
        size_t nReturnPos = strValue.rfind('\r');
        if (std::string::npos != nReturnPos) {
            strValue.resize(nReturnPos);
        }

        pHeaderContent->insert(std::make_pair(strKey, strValue));
    }

    return realSize;
}

size_t FileUtil::getContentLength(const std::string &url, std::map<std::string, std::string> &headerInfo) {
    CURL *handle = curl_easy_init();
    if (NULL == handle) {
        return (size_t) (0);
    }

    headerInfo.clear();

    curl_easy_setopt(handle, CURLOPT_URL, url.c_str());
    curl_easy_setopt(handle, CURLOPT_HEADER, 1L);    // 需要将标头传递到数据流

    // 设置头部写入回调
    curl_easy_setopt(handle, CURLOPT_HEADERFUNCTION, FileUtil::headerCallback);
    curl_easy_setopt(handle, CURLOPT_HEADERDATA, (void *) &headerInfo);

    curl_easy_setopt(handle, CURLOPT_NOBODY, 1L);    // 不需要body
    curl_easy_setopt(handle, CURLOPT_SSL_VERIFYPEER, 0L);
    curl_easy_setopt(handle, CURLOPT_SSL_VERIFYHOST, 0L);
    curl_easy_setopt(handle, CURLOPT_FOLLOWLOCATION, 1L);
    curl_easy_setopt(handle, CURLOPT_VERBOSE, 0L);

    curl_off_t downloadFileLenth = 0;
    CURLcode curlCode = CURLE_OK;

    // 重试几次, 可能会出现超时CURLE_OPERATION_TIMEDOUT
    for (int i = 0; i < 10; i++) {
        curlCode = curl_easy_perform(handle);
        if (CURLE_OK == curlCode) {
            curlCode = curl_easy_getinfo(handle, CURLINFO_CONTENT_LENGTH_DOWNLOAD_T, &downloadFileLenth);
            long response_code = 0;
            curlCode = curl_easy_getinfo(handle, CURLINFO_RESPONSE_CODE, &response_code);
            if (response_code < 200 || response_code >= 300) {
                downloadFileLenth = 0;
            }

            break;
        }

        std::this_thread::sleep_for(std::chrono::milliseconds(50));
    }

    (void) curl_easy_cleanup(handle);

    return (size_t) downloadFileLenth;
}

bool FileUtil::isSupportAcceptRanges(const std::map<std::string, std::string> &headerInfo) {
    auto itKey = headerInfo.find("Accept-Ranges");
    if (itKey != headerInfo.end()) {
        if (itKey->second == "none") {
            return false;
        }

        // 检查是否支持分段下载(或断点续传)
        if (itKey->second == "bytes") {
            return true;
        }
    }

    return false;
}

size_t FileUtil::getContentLength(const std::string &url) {
     std::map<std::string, std::string> headerInfo;
    return getContentLength(url, headerInfo);
}