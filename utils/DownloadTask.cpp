#include <iostream>
#include <future>
#include <thread>
#include <fcntl.h>
#include "Util.h"
#include "FileUtil.h"
#include "DownloadTask.h"

DownloadTask::DownloadTask()
        : threadCount(0) {
    threadNum = 1;
    isCancel = false;
    isDownloading = false;
    downloadedSize = 0;
    totalSize = 0;
    isDownloadSuccess = false;
}

DownloadTask::DownloadTask(const DownloadTask &obj)
        : url(obj.url), dest(obj.dest), threadCount(0) {
    isCancel = false;
    isDownloading = false;
    downloadedSize = 0;
    totalSize = 0;
    isDownloadSuccess = false;
    threadNum = obj.threadNum;
}

DownloadTask::DownloadTask(const DownloadConfiguration config) : threadCount(0) {
    isCancel = false;
    url = config.getValue("url",
                          "https://d1.music.126.net/dmusic/NeteaseCloudMusic_Music_official_2.10.13.202675_32.exe");
    dest = config.getValue("dest", "wangyiyun.exe");
    isDownloading = false;
    downloadedSize = 0;
    totalSize = 0;
    int threadNumber = std::stoi(config.getValue("threadNum", "1"));
    if (threadNumber <= 0) {
        threadNum = 1;
    } else {
        threadNum = threadNumber;
    }
}

DownloadTask::~DownloadTask() {
    (void) cancelDownload();
}

bool DownloadTask::downloadByParts(long long totalSize, long long packageSize, int threadNum) {
    std::string strTmpFile = dest + ".tmp";
    bool bDownloadFinish = true;

    do {
        /* Delete files to prevent naming conflicts  */
        if (unlink(dest.c_str()) == -1) {
            if (ENOENT != errno) {
                bDownloadFinish = false;
                break;
            }
        }

        /* Temporary files need to be cleared */
        (void) FileUtil::truncateFile(strTmpFile);

        /*
         * The strategy adopted here is to divide the file into n blocks (n represents the number of threads),
         * and allocate the starting and ending points for writing the file for each thread,
         * so as to facilitate multi-threaded file downloading.
         */
        for (int i = 0; i < threadNum; i++) {
            PackagePartition *packagePartition = createPackage(i, packageSize, totalSize, strTmpFile);
            if (nullptr == packagePartition) {
                cancelDownload();
                break;
            }

            parts.push_back(packagePartition);

            /* The working function of threads */
            auto workFunc = [this, packagePartition]() -> void {
                packagePartition->downloadData();

                /*
                 * The download task is completed, with threadCount decreasing by 1.
                 * As theadCount is an atomic variable, it can be used to check if
                 * all threads have completed their download tasks.
                 */
                threadCount--;
            };

            std::thread downloadThread = std::thread(workFunc);

            try {
                downloadThread.detach();
            } catch (...) {
                std::cerr << "downloadTask excetion" << std::endl;
                std::terminate();
            }

            threadCount++;
        }
    } while (false);

    /*
     * Wait for all threads to complete, and use the progress printing function
     * to print the download progress during the process.
     */
    while (threadCount) {
        showProgress(downloadedSize, totalSize, (double) downloadedSize / totalSize);
        std::this_thread::sleep_for(std::chrono::milliseconds(20));
    }
    showProgress(downloadedSize, totalSize, (double) downloadedSize / totalSize);
    std::cout << std::endl;

    /*
     * Check the results of all download threads, and if there is a failure,
     * it is considered that the download task has failed.
     */
    for (auto &item: parts) {
        if (!item->isSuccess1()) {
            bDownloadFinish = false;
        }
        delete item;
    }

    parts.clear();

    isDownloadSuccess = bDownloadFinish;
    isCancel = false;

    /* Change the file name of a temporary file. */
    if (bDownloadFinish) {
        rename(strTmpFile.c_str(), dest.c_str());
    }

    return bDownloadFinish;
}

void DownloadTask::syncDownload() {
    long long packageSize = 0;
    bool isSuccess = false;

    // 等待下载线程结束
    (void) waitForDownloadFinish();

    CURL_HEADER_INFO headerInfo;

    // 获取内容大小
    totalSize = FileUtil::getContentLength(url, headerInfo);

    // 返回0表示失败
    if (((size_t) 0) == totalSize) {
        isDownloading = false;
        isDownloadSuccess = false;
        std::cout << "cannot get the file size, download file: \"" << this->dest << "\" failed!" << std::endl;
        return;
    }

    int threadNumber = threadNum;

    //文件大小未知则仅单线程下载
    if ((size_t) (-1) == totalSize) {
        threadNumber = 1;
        packageSize = (size_t) (-1);
    } else {
        if (totalSize <= Util::MINIMUM_PACKAGE_SIZE) {
            threadNumber = 1;
        } else {
            int maxThreadNumber = totalSize / Util::MINIMUM_PACKAGE_SIZE;
            threadNumber = maxThreadNumber < threadNum ? maxThreadNumber : threadNum;
        }

        if (threadNumber != threadNum) {
            std::cout << "too many thread, just need " << threadNumber << " thread." << std::endl;
        }

        // 获取分包大小
        packageSize = totalSize / threadNumber;
    }

    isDownloading = true;
    isSuccess = downloadByParts(totalSize, packageSize, threadNumber);
    isDownloading = false;
    if (!isSuccess) {
        char msg[Util::MAX_MSG_SIZE];
        snprintf(msg, Util::MAX_MSG_SIZE, "download file \"%s\" failed.", this->dest.c_str());
        std::cerr << msg << std::endl;
    }
}

bool DownloadTask::IsDownloading() const {
    return isDownloading;
}

bool DownloadTask::getLastDownloadResult() const {
    return isDownloadSuccess;
}

void DownloadTask::cancelDownload() {
    isCancel = true;
    for (auto &item: parts) {
        item->setIsCancel(true);
    }
    (void) waitForDownloadFinish();
}

void DownloadTask::waitForDownloadFinish() const {
    while (isDownloading) {
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }
}

long long DownloadTask::getDownloadedSize() const {
    return downloadedSize;
}

long long DownloadTask::getTotalSize() const {
    return totalSize;
}

/* Update download progress. */
int DownloadTask::updateProgress(long long totalSize, long long downloadedSize) {
    long long download = 0;

    for (auto &item: parts) {
        download += item->getDownloadSize();
    }

    this->downloadedSize = download;

    if (isCancel) {
        return -1;
    }

    return CURLE_OK;
}


void DownloadTask::showProgress(long long downloadedSize, long long totalSize, double progress) {
    char msg[Util::MAX_MSG_SIZE];
    snprintf(msg, Util::MAX_MSG_SIZE, "%.2lf%% %lld/%lld", progress * 100, downloadedSize, totalSize);
    std::cout << "\r" << msg;
}

PackagePartition *
DownloadTask::createPackage(const int packageId, const int packageSize, const int totalSize,
                            const std::string filename) {
    int startPos = packageId * packageSize;
    int endPos = 0;
    int taskSize = 0;
    if ((size_t) (-1) == totalSize) {
        endPos = totalSize;
        taskSize = totalSize;
    } else {
        /* The last block is directly allocated to the end of the file */
        endPos = (packageId < threadNum - 1) ? ((packageId + 1) * packageSize - 1) : (totalSize - 1);
        taskSize = (endPos - startPos) + 1;
    }
    PackagePartition *packagePartition = new(std::nothrow) PackagePartition(packageId, url, this, startPos,
                                                                            endPos, taskSize, filename);
    return packagePartition;
}

CURL *HttpDownloadTask::createCurl(PackagePartition *packageInfo) {
    if (nullptr == packageInfo) {
        return nullptr;
    }

    CURL *curl = curl_easy_init();
    if (nullptr == curl) {
        return nullptr;
    }

    curl_easy_setopt(curl, CURLOPT_URL, packageInfo->getUrl().c_str());                   // set the url of download task
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writeCallback);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, static_cast<void *>(packageInfo));
    curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 0L);
    curl_easy_setopt(curl, CURLOPT_XFERINFOFUNCTION, progressCallback);
    curl_easy_setopt(curl, CURLOPT_XFERINFODATA, static_cast<void *>(packageInfo));
    curl_easy_setopt(curl, CURLOPT_NOSIGNAL, 1L);
    curl_easy_setopt(curl, CURLOPT_CONNECTTIMEOUT, 60);                             // set the timeout of connect
    curl_easy_setopt(curl, CURLOPT_TIMEOUT, 0L);                                    // set the timeout of transform data
    curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);                             // ignore the ssl verifier
    curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L);
    curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
    curl_easy_setopt(curl, CURLOPT_VERBOSE, 0L);

    // set the range of data that packagePartition want to download
    char range[Util::MAX_PATH_SIZE] = {0};

    if ((size_t) (-1) == packageInfo->getEndPos()) {
        packageInfo->setStartPos(0);
        curl_easy_setopt(curl, CURLOPT_RANGE, 0L);
    } else {
        snprintf(range, sizeof(range), "%lld-%lld", packageInfo->getStartPos(), packageInfo->getEndPos());
        curl_easy_setopt(curl, CURLOPT_RANGE, range);
    }

    return curl;
}

HttpDownloadTask::HttpDownloadTask(const DownloadConfiguration config) : DownloadTask(config) {

}

size_t HttpDownloadTask::writeCallback(void *data, size_t size, size_t nmemb, void *packageData) {
    size_t realSize = size * nmemb;
    if (NULL == data || NULL == packageData) {
        return realSize;
    }

    PackagePartition *packagePartition = static_cast<PackagePartition *>(packageData);

    size_t dwWritten = FileUtil::writeFile(packagePartition->getFile(), data, (unsigned long) realSize);
    packagePartition->addDownloadSize(dwWritten);
    packagePartition->addStartPos(dwWritten);
    return dwWritten;
}

int HttpDownloadTask::progressCallback(void *data, curl_off_t totalSize, curl_off_t downloadedSize) {
    if (NULL == data || 0 == totalSize) {
        return CURLE_OK;
    }

    PackagePartition *packagePartition = static_cast<PackagePartition *>(data);
    DownloadTask *pThis = packagePartition->getDownloadTask();

    return pThis->updateProgress(totalSize, downloadedSize);
}