#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <map>
#include <memory>
#include "Util.h"
#include "SimpleDownloadTaskFactory.h"
#include "DownloadConfiguration.h"
#include "DownloadManager.h"

int main(int argc, char* argv[]) {
    if (argc != 2) {  
        std::cerr << "Usage: " << argv[0] << " <config_file_path>" << std::endl;
        return 1;
    }

    std::string configFilePath = argv[1];  
    DownloadConfiguration config(configFilePath);
    DownloadManager mgr;
    std::shared_ptr<DownloadTask> task = SimpleDownloadTaskFactory::createTask(config);
    if (task == nullptr) {
        return 1;
    }
    mgr.addDownloadTask(task);
    (void)mgr.startDownload();
    return 0;
}
