#ifndef DOWNLOADER_DOWNLOADCONFIGURATION_H
#define DOWNLOADER_DOWNLOADCONFIGURATION_H
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <map>
class DownloadConfiguration {
public:
    DownloadConfiguration(const std::string& filename);
    std::string getValue(const std::string& key, const std::string& defaultValue = "") const;

private:
    std::map<std::string, std::string> configData;
public:
    const std::map<std::string, std::string> &getConfigData() const;

private:
    void parseLine(const std::string& line);
};


#endif //DOWNLOADER_DOWNLOADCONFIGURATION_H
