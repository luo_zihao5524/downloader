#ifndef DOWNLOADER_SIMPLE_DOWNLOAD_FACTORY_H
#define DOWNLOADER_SIMPLE_DOWNLOAD_FACTORY_H
#include <string>
#include "DownloadTask.h"
#include "DownloadConfiguration.h"

class SimpleDownloadTaskFactory {  
public:  
    static std::shared_ptr<DownloadTask> createTask(const DownloadConfiguration config) {
        std::string protocol = config.getValue("protocol", "http");
        if (protocol == "http") {  
            return std::make_shared<HttpDownloadTask>(config);
        } else {
            std::cerr << "Cannot create download task because of uknown protocol: \"" << protocol << "\"." << std::endl;
            std::cerr << "please check your config file!" << std::endl;
        }
        return nullptr;  
    }  
};
#endif