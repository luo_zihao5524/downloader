#ifndef DOWNLOADER_DOWNLOAD_MANAGERR_H
#define DOWNLOADER_DOWNLOAD_MANAGERR_H
#include "DownloadTask.h"

class DownloadManager {
public:

    DownloadManager();

    ~DownloadManager();

    void addDownloadTask(std::shared_ptr<DownloadTask> task);

    void clear();

    bool startDownload();

    void cancelDownload();

private:
    // 休眠
    void sleepMillisecond(int millisecond) const;

private:
    std::vector<std::shared_ptr<DownloadTask>> tasks;         // download task queue
    bool m_IsCancel;                                          // is cancel download
};
#endif