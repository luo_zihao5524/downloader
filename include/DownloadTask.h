#ifndef DOWNLOADER_DOWNLOAD_TASK_H
#define DOWNLOADER_DOWNLOAD_TASK_H
#include <vector>
#include <atomic>
#include <functional>
#include <map>
#include <curl/curl.h>
#include <string>
#include "PackageDownload.h"
#include "DownloadConfiguration.h"

//请求头信息
using CURL_HEADER_INFO = std::map<std::string, std::string>;

//单个文件下载
class DownloadTask {
public:
    DownloadTask();

    DownloadTask(const DownloadTask &obj);

    DownloadTask(const DownloadConfiguration config);

    virtual ~DownloadTask() = 0;

    // download files by synchronization
    void syncDownload();

    void cancelDownload();

    void waitForDownloadFinish() const;

    bool IsDownloading() const;

    // get the last result of download
    bool getLastDownloadResult() const;

    long long getDownloadedSize() const;

    long long getTotalSize() const;

    // update the progress information of task
    int updateProgress(long long totalSize, long long downloadedSize);

    virtual CURL* createCurl(PackagePartition *packageInfo) = 0;

private:
    // download file by parts
    bool downloadByParts(long long totalSize, long long packageSize, int threadNum);

    void showProgress(long long downloadedSize, long long totalSize, double progress);

    PackagePartition* createPackage(const int packageId, const int packageSize, const int totalSize, const std::string fileName);

private:
    std::string url;
    std::string dest;
    std::vector<PackagePartition *> parts;
    std::atomic<int> threadCount;
    int threadNum;
    bool isCancel;
    bool isDownloading;
    bool isDownloadSuccess;
    long long downloadedSize;
    long long totalSize;
};

class HttpDownloadTask : public DownloadTask {
public:
    CURL* createCurl(PackagePartition *packageInfo) override;

    HttpDownloadTask(const DownloadConfiguration config);

private:
    static size_t writeCallback(void *data, size_t size, size_t nmemb, void *packageData);

    static int progressCallback(void *data, curl_off_t totalSize, curl_off_t downloadedSize);
};

#endif
