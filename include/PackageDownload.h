#ifndef DOWNLOADER_PACKAGEDOWNLOAD_H
#define DOWNLOADER_PACKAGEDOWNLOAD_H
class DownloadTask;
class PackagePartition {
public:
    PackagePartition(const int packageId, std::string url, DownloadTask *task, int startPos, int endPos,
                     int totalSize, const std::string tmpFileName);
    bool isSuccess1() const;

    DownloadTask *getDownloadTask() const;

    int getFile() const;

    long long int getStartPos() const;

    void setStartPos(long long int startPos);

    long long int getEndPos() const;

    long long int getDownloadSize() const;

    bool downloadData();

    const std::string &getUrl() const;

    void setIsCancel(bool isCancel);

    void addDownloadSize(long long int downloadSize);

    void addStartPos(long long int startPos);

private:

    // call back function of write
    static size_t writeCallback(void *data, size_t size, size_t nmemb, void *packageData);

    // call back function of progress
    static int progressCallback(void *data, curl_off_t totalSize, curl_off_t downloadedSize);

private:
    int packageId;                // package id
    std::string url;              // download address
    std::string tmpFileName;      // the file name of tmp file
    DownloadTask *downloadTask;   // the download task
    int file;                     // File handle
    long long startPos;           // the start position of writing file
    long long endPos;             // the end position of writing file
    long long downloadSize;       // size of downloaded file
    long long totalSize;          // total size of the file
    void *curl;
    bool isCancel;                // is download cancel
    bool isSuccess;               // is download success
};
#endif //DOWNLOADER_PACKAGEDOWNLOAD_H
