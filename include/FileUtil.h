#ifndef DOWNLOADER_FILEUTIL_H
#define DOWNLOADER_FILEUTIL_H
#include <string>
#include <fcntl.h>
#include <unistd.h>
#include <map>

class FileUtil {
public:
    static void truncateFile(const std::string &path);

    static size_t writeFile(int file, const void *buffer, unsigned long size);

    static size_t getContentLength(const std::string &url, std::map<std::string, std::string> &headerInfo);

    static size_t getContentLength(const std::string &url);

    static bool isSupportAcceptRanges(const std::map<std::string, std::string> &headerInfo);

private:
    static size_t headerCallback(char *buffer, size_t size, size_t itemsNum, void *packageData);
};


#endif //DOWNLOADER_FILEUTIL_H
