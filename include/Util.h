#ifndef DOWNLOADER_UTIL_H
#define DOWNLOADER_UTIL_H

class Util {
public:
    const static int FILE_START_POS = 0;    // The offset used when writing a file, representing writing from the beginning of the file

    const static int INVALID_FD = -1;   // Invalid fd

    const static int MAX_PATH_SIZE = 1024;

    const static int MAX_MSG_SIZE = 1024;

    const static int MINIMUM_PACKAGE_SIZE = 16 * 1024 * 1024;
};

#endif //DOWNLOADER_UTIL_H
