**使用方法**
```
# 确保环境中安装了libcurl，如果未安装则在root用户下以下面的命令进行安装
yum install -y libcurl-devel

# 完成之后直接在downloader目录下执行如下命令即可生成可执行文件downloader
make

# 提供一个可读的配置文件运行即可，代码中已有一个简单的配置文件，所以直接运行如下命令即可
./downloader example_config.ini

# 提供一个memcheck编译选项作为内存检查版本downloader_memcheck
make memcheck

# 生成一个可执行文件downloader_memcheck，用于检查是否有内存泄漏
./downloader_memcheck example_config.ini
```